<p><a href="<?php echo site_url('Admin'); ?>">back</a></p>
<table width="30%" id="dataTable" cellspacing="0">
    <thead>
        <tr>
            <th>No</th>
            <th>Tujuan Pengeluaran</th>
            <th>Nominal</th>
            <th>Tanggal Pengeluaran</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; 
            foreach ($spendingData as $spending) { ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $spending->spending_for; ?></td>
                <td><?php echo $spending->nominal_spending; ?></td>
                <td><?php echo $spending->date_spending; ?></td>
            </tr>
        <?php }?>
    </tbody>
</table>
