<p><a href="<?php echo site_url('Admin'); ?>">back</a></p>
<table width="30%" id="dataTable" cellspacing="0">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Donatur</th>
            <th>Nominal</th>
            <th>Tanggal Donasi</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; 
            foreach ($incomeData as $income) { ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $income->donatur_name; ?></td>
                <td><?php echo $income->nominal_income; ?></td>
                <td><?php echo $income->date_income; ?></td>
            </tr>
        <?php }?>
    </tbody>
</table>
