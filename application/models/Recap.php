<?php
	class Recap extends CI_Model
    {
        public $tableName;

        function __construct()
		{
            parent::__construct();
            $this->tableName = "tbrecap";
        }
        
        function getLastbalance()
		{
			$this->db->select('*');
			$this->db->from($this->tableName);
            $this->db->order_by('id_recap', 'DESC');
            $this->db->limit(1);
			return $this->db->get()->row('total_balance');
        }
        
        function inputBalance($data){
			$this->db->insert($this->tableName, $data);
		}
    }
    