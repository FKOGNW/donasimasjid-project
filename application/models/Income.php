<?php
	class Income extends CI_Model
    {
        public $tableName;

        function __construct()
		{
            parent::__construct();
            $this->tableName = "tbincome";
        }

        function getAllIncome()
		{
			$this->db->select('*');
			$this->db->from($this->tableName);
			$this->db->order_by('id_income', 'ASC');
			return $this->db->get()->result();
        }
        
        function getLastTotalIncome()
		{
			$this->db->select('*');
			$this->db->from($this->tableName);
            $this->db->order_by('id_income', 'DESC');
            $this->db->limit(1);
			return $this->db->get()->row('total_income');
        }
        
        function inputIncome($data){
			$this->db->insert($this->tableName, $data);
		}
    }
    