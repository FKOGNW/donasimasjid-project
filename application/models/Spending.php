<?php
	class Spending extends CI_Model
    {
        public $tableName;

        function __construct()
		{
            parent::__construct();
            $this->tableName = "tbspending";
        }

        function getAllSpending()
		{
			$this->db->select('*');
			$this->db->from($this->tableName);
			$this->db->order_by('id_spending', 'ASC');
			return $this->db->get()->result();
        }
        
        function getLastTotalSpending()
		{
			$this->db->select('*');
			$this->db->from($this->tableName);
            $this->db->order_by('id_spending', 'DESC');
            $this->db->limit(1);
			return $this->db->get()->row('total_spending');
        }
        
        function inputSpending($data){
			$this->db->insert($this->tableName, $data);
		}
    }
    