<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function index()
	{
		$data['dataIncome']=$this->Income->getLastTotalIncome();
		$data['dataSpending']=$this->Spending->getLastTotalSpending();
		$data['dataBalance']=$this->Recap->getLastBalance();
		$this->load->view('user/dashboard', $data);
	}
}
