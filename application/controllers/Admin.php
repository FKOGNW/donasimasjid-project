<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function index()
	{
        if(!$this->session->userdata('logged_in')) {
            $this->load->view('admin/login');
		}else{
			redirect('Admin/adminDashboard');
		}

    }
    
    public function login()
	{
        if($this->User->checkLogin($this->input->post('username'),md5($this->input->post('password')))){
			$userdata = array(
				'username'  => $this->input->post('username'),		
				'logged_in' => true
			);

			/*insert date and time user login*/
			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
			$data['last_login']= $now->format('Y-m-d H:i:s');
			$data['username']= $this->input->post('username');
			$this->User->lastLogin($data);

			$this->session->set_userdata($userdata);
			
            redirect(site_url('Admin/adminDashboard'));
		}else{
			redirect('Admin');
		}

    }

    public function logout() {
		$this->session->sess_destroy();
		redirect('Admin');
	}



    public function adminDashboard()
	{
        if(!$this->session->userdata('logged_in')) {
            redirect('Admin');
		}else{
            $data['dataIncome']=$this->Income->getLastTotalIncome();
            $data['dataSpending']=$this->Spending->getLastTotalSpending();
            $data['dataBalance']=$this->Recap->getLastBalance();
            $this->load->view('admin/dashboard', $data);
		}
    }

    public function incomeInputPage()
	{
        if(!$this->session->userdata('logged_in')) {
            redirect('Admin');
		}else{
            $this->load->view('admin/inputIncomePage');
		}
    }

    public function inputIncome()
	{
        if(!$this->session->userdata('logged_in')) {
            redirect('Admin');
		}else{
            $lastTotalIncome= $this->Income->getLastTotalIncome();
            $lastTotalSpending= $this->Spending->getLastTotalSpending();

            $totalIncome= $this->input->post('nominal_income')+$lastTotalIncome;
            $data= array(
                'donatur_name'=> $this->input->post('donatur_name'),
                'nominal_income'=> $this->input->post('nominal_income'),
                'date_income'=> $this->input->post('date_income'),
                'total_income'=> $totalIncome
            );

            $this->Income->inputIncome($data);

            $balance= $totalIncome - $lastTotalSpending;
            $now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
            $dataBalance= array(
                'total_balance'=>$balance,
                'last_update'=>$now->format('Y-m-d H:i:s')
            );
            $this->Recap->inputBalance($dataBalance);
            redirect('Admin');
		}
    }

    public function incomePage()
	{
        if(!$this->session->userdata('logged_in')) {
            redirect('Admin');
		}else{
            $data['incomeData']= $this->Income->getAllIncome();
            $this->load->view('admin/incomePage', $data);
		}
    }

    public function spendingInputPage()
	{
        if(!$this->session->userdata('logged_in')) {
            redirect('Admin');
		}else{
            $this->load->view('admin/inputSpendingPage');
		}
    }

    public function inputSpending()
	{
        if(!$this->session->userdata('logged_in')) {
            redirect('Admin');
		}else{
            $lastTotalSpending= $this->Spending->getLastTotalSpending();
            $lastTotalIncome= $this->Income->getLastTotalIncome();

            $totalSpending= $this->input->post('nominal_spending')+$lastTotalSpending;
            $data= array(
                'spending_for'=> $this->input->post('spending_for'),
                'nominal_spending'=> $this->input->post('nominal_spending'),
                'date_spending'=> $this->input->post('date_spending'),
                'total_spending'=> $totalSpending
            );

            $this->Spending->inputSpending($data);

            $balance= $lastTotalIncome - $totalSpending;
            $now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
            $dataBalance= array(
                'total_balance'=>$balance,
                'last_update'=>$now->format('Y-m-d H:i:s')
            );
            $this->Recap->inputBalance($dataBalance);

            redirect('Admin');
		}
    }

    public function spendingPage()
	{
        if(!$this->session->userdata('logged_in')) {
            redirect('Admin');
		}else{
            $data['spendingData']= $this->Spending->getAllSpending();
            $this->load->view('admin/spendingPage', $data);
		}
    }
    
}
